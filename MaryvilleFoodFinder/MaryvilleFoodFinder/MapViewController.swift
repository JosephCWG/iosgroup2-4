//
//  FirstViewController.swift
//  MaryvilleFoodFinder
//
//  Created by Joseph on 2/14/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import UIKit
import MapKit

// Because Swift is horrible // Joey
extension String {
    subscript(_ range: CountableRange<Int>) -> String {
        let idx1 = index(startIndex, offsetBy: max(0, range.lowerBound))
        let idx2 = index(startIndex, offsetBy: min(self.count, range.upperBound))
        return String(self[idx1..<idx2])
    }
}

class MapViewController: UIViewController, MKMapViewDelegate {
    
    let locManager = CLLocationManager()
    var selectedPin:String = ""
    var currentSettings:[Restaurant]?
    @IBOutlet weak var locationMapView: MKMapView!
    
    // Remove all the pins from the map, and then redisplay the desired ones.
    @objc private func updateDisplayedMapPins() {
        locationMapView.removeAnnotations(locationMapView!.annotations)
        self.addRestaurantsToMap(listOfRestaurants: Restaurants.shared.visibleRestaurants)
        locationMapView.reloadInputViews()
    }
    
    // Give the ability to click on pins and link to the restaurant details view. // Randall
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: -5, y: 5)
        let detailBtn:UIButton = UIButton(type: .detailDisclosure)
        print("mapView Annotation.title: \(annotation.title!!)")
        detailBtn.setTitle(annotation.title ?? "", for: [])
        detailBtn.addTarget(self, action: #selector(MapViewController.showDetails), for: .touchUpInside)
        view.rightCalloutAccessoryView = detailBtn
        return view
    }
    
    // Link to the restaurant details page
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let restaurantDVC = segue.destination as! RestaurantDetailViewController
        restaurantDVC.restaurant = Restaurants.shared.findRestaurantByName(name: selectedPin)
    }
    
    // Pin is clicked on from the map // Randall
    @objc func showDetails(_ sender: Any){
        selectedPin = (sender as! UIButton).currentTitle ?? "NO CURRENT TITLE"
        performSegue(withIdentifier: "detailsFromMap", sender:self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add an observer so that we can update the map from the singleton.
        let name = Notification.Name("UpdateDisplayedMapPins")
        NotificationCenter.default.addObserver(self, selector: #selector(updateDisplayedMapPins), name: name, object: nil)
        // Download points of interest
        Restaurants.downloadRestaurants()
        // Set the map to default center around Maryville.
        setMapViewRegion(
            centerCoordinate: CLLocationCoordinate2D(latitude: 40.33831, longitude: -94.872950),
            displayWidthInMeters: 5000, displayHeightInMeters: 5000)
        
            }
    
    // Set the region to display on the map. // Joey
    // It was intended originally that when you click on the pin it would zoom in the map to that pin.
    func setMapViewRegion(centerCoordinate:CLLocationCoordinate2D, displayWidthInMeters: Double = 300, displayHeightInMeters: Double = 300) {
        // Center the map and set the zoom level
        locationMapView.setRegion(
            MKCoordinateRegion(
                center: centerCoordinate,
                latitudinalMeters: CLLocationDistance(displayWidthInMeters),
                longitudinalMeters: CLLocationDistance(displayHeightInMeters)),
            animated: true)
    }
    
    // Pass in a restaurant and it will have a pin for it added to the map.
    // Joey
    func addRestaurantToMap(restaurant: Restaurant) {
        let annotation = MKPointAnnotation()
        annotation.title = restaurant.name
        annotation.coordinate = restaurant.twoDimensionalCoordinate
        locationMapView.addAnnotation(annotation)
    }
    
    // Joey
    func addRestaurantsToMap(listOfRestaurants:[Restaurant]) {
        for pointOfInterest in listOfRestaurants {
            addRestaurantToMap(restaurant: pointOfInterest)
        }
    }
    
    // Joey
    public func alertUser(strTitle: String, strMessage: String, viewController: UIViewController) {
        let myAlert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        myAlert.addAction(okAction)
        viewController.present(myAlert, animated: true, completion: nil)
    }
    
    // Logan // Joey
    func displayOnlyOpenRestaurants() {
        // Check if custom filters are in use.
        if currentSettings != nil {
            Restaurants.shared.visibleRestaurants = currentSettings!
        }

        let openRestaurants = Restaurants.shared.visibleRestaurants.filter({
            return Restaurants.isRestaurantOpen(restaurant: $0)
        })
        
        Restaurants.shared.visibleRestaurants = openRestaurants
        Restaurants.shared.reloadMapView()
    }
    
    @IBOutlet weak var openRestaurantOnlySwitch: UISwitch!
    @IBAction func openRestaurantsOnly(_ sender: Any) {
        // If the open restaurants only button is set to on...
        if openRestaurantOnlySwitch.isOn {
            displayOnlyOpenRestaurants()
        }else{
            if currentSettings != nil {
                Restaurants.shared.visibleRestaurants = currentSettings!
            }
            Restaurants.shared.reloadMapView()
        }
    }
}
