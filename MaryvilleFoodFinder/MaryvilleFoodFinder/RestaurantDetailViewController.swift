//
//  RestaurantDetailViewController.swift
//  MaryvilleFoodFinder
//
//  Created by Student on 3/11/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import UIKit
import MapKit

class RestaurantDetailViewController: UIViewController {
    
    @IBOutlet weak var getRatingLBL: UILabel!
    @IBOutlet weak var getTypeLBL: UILabel!
    @IBOutlet weak var restaurantImage: UIImageView!
    @IBOutlet weak var descriptionTV:UITextView!
    var restaurant:Restaurant!
    
    // On load set all of the labels along with the image and details for the restaurant.
    override func viewDidLoad() {
        super.viewDidLoad()
        getTypeLBL.text = "\(restaurant.type.name)"
        getRatingLBL.text = "\(restaurant.rating)"
        restaurantImage.image = UIImage.init(named: restaurant.image)
        //self.navigationItem.title = restaurant.name
        if restaurant.descText != "" {
            descriptionTV.text = restaurant.descText
        }
    }
    
    // When the navigate button is pressed then open the location in apple maps.
    @IBAction func navigateToLocation(_ sender: Any) {
        // This was originally taken from https://stackoverflow.com/questions/28604429/how-to-open-maps-app-programmatically-with-coordinates-in-swift
        let latitude: CLLocationDegrees = restaurant.lat
        let longitude: CLLocationDegrees = restaurant.lon
        
        let regionDistance:CLLocationDistance = 4000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(restaurant.name)"
        mapItem.openInMaps(launchOptions: options)
    }
    
    // When the call button is pressed, open a call dialogue
    @IBAction func callButtonPressed(_ sender: Any) {
        // This was taken from https://stackoverflow.com/questions/27259824/calling-a-phone-number-in-swift
        // .open(url) was deprecated past ios 10.
        if let url = URL(string: "tel://\(restaurant.phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
  
    // Provide a safari redirect to the restaurant's website so you can view the menu
    @IBAction func menu(_ sender: Any) {
         openUrl(url: "\(restaurant.menuLink)")
    }
    
    func openUrl(url:String!) {
        if let url = NSURL(string:url) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
