//
//  SettingsViewController.swift
//  MaryvilleFoodFinder
//
//  Created by Randall Zane Porter on 2/24/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class SettingsViewController: UIViewController, CLLocationManagerDelegate {
    
    //LWS BEG
    var locationManager = CLLocationManager()
    var myLocation:CLLocation?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        currentLocation()
    }
    
    
    @IBOutlet weak var distanceLBL: UILabel!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var minLBL: UILabel!
    @IBOutlet weak var maxLBL: UILabel!
    @IBOutlet weak var maxDistanceSlider: UISlider!
    @IBOutlet weak var minRatingSlider: UISlider!
    
    //RZP 02-24-19 BEG
    @IBOutlet weak var minOutlet: UISlider!
    @IBOutlet weak var maxOutlet: UISlider!
    // These two functions prevent logical errors
    // Need these functions to make sure the max
    //slider is always greater than the min slider
    @IBAction func raiseMaxPrice(_ sender: AnyObject){
        let fixedMax = roundf(sender.value)
        sender.setValue(fixedMax, animated: true)
        maxLBL.text = String(Int(maxOutlet.value))

        let minPrice = minOutlet.value
        let maxPrice = maxOutlet.value
        if minPrice > maxPrice {
            maxOutlet.value = minPrice + 1
        }
        let fixedMin = roundf(sender.value)
        sender.setValue(fixedMin, animated: true)
        minLBL.text = String(Int(minOutlet.value))
    }
    
    // Lower the MinPriceSlider
    @IBAction func lowerMinPrice(_ sender: AnyObject) {
        let fixed = roundf(sender.value)
        sender.setValue(fixed, animated: true)
        minLBL.text = String(minOutlet.value)
        let minPrice = minOutlet.value
        let maxPrice = maxOutlet.value
        if minPrice > maxPrice {
            minOutlet.value = maxPrice - 1
        }
        let fixedMax = roundf(sender.value)
        sender.setValue(fixedMax, animated: true)
        maxLBL.text = String(Int(maxOutlet.value))
    }
    //RZP 02-24-19 END
    
    //Sets labels equal to slider values
    @IBAction func distSliderChange(_ sender: AnyObject) {
        let fixed = roundf(sender.value)
        sender.setValue(fixed, animated: true)
        distanceLBL.text = String(Int(maxDistanceSlider.value))
    }
    
    @IBAction func ratingSliderChange(_ sender: AnyObject) {
        let fixed = roundf(sender.value)
        sender.setValue(fixed, animated: true)
        ratingLBL.text = String(Int(minRatingSlider.value))
    }
    
    //gets permission for location and sets up
    func currentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        //Gets User location for distance calculation
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    //gets location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        myLocation = locations[0] as CLLocation
        
    }
    //Permission Denied
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("Failed to retrieve location")
    }
    //Resets visible restaurants to all and resets values
    @IBAction func resetBTN(_ sender: Any) {
        Restaurants.shared.visibleRestaurants = Restaurants.shared.allRestaurants
        Restaurants.shared.reloadMapView()
        maxDistanceSlider.value = 5
        distanceLBL.text = String(Int(maxDistanceSlider.value))
        minRatingSlider.value = 0
        ratingLBL.text = String(Int(minRatingSlider.value))
        minOutlet.value = 0
        minLBL.text = String(Int(minOutlet.value))
        maxOutlet.value = 10
        maxLBL.text = String(Int(maxOutlet.value))
        
    }
    
    //Submits filters to map view
    @IBAction func submitSettings(_ sender: Any) {
        
        //filter for distance from restaurant converted
        var acceptableRestaurants:[Restaurant] = []
        for r in Restaurants.shared.visibleRestaurants{
            let restaurantsLoc = CLLocation(latitude: r.lat, longitude: r.lon)
        
            if restaurantsLoc.distance(from: myLocation!) < (Double(maxDistanceSlider.value) * 1609.344) {
                acceptableRestaurants.append(r)
            }
        }
        
        Restaurants.shared.visibleRestaurants = acceptableRestaurants
        let ratingFilter = Restaurants.shared.visibleRestaurants.filter({$0.rating > Double(minRatingSlider.value)})
        let lowPriceFilter = ratingFilter.filter({$0.price > Double(minOutlet.value)})
        let highPriceFilter = lowPriceFilter.filter({$0.price < Double(maxOutlet.value)})
    
        Restaurants.shared.visibleRestaurants = highPriceFilter
        Restaurants.shared.reloadMapView()
    }
    //Passes settings to mapViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let mapVC = segue.destination as! MapViewController
        mapVC.currentSettings = Restaurants.shared.visibleRestaurants
    }
    //LWS END
}
