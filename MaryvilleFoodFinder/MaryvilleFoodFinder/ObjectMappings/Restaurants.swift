//
//  Restaurants.swift
//  MaryvilleFoodFinder
//
//  Created by Joseph on 3/15/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import Foundation

class Restaurants {
    // Singleton instance of our restaurants
    public static var shared = Restaurants()
    var allRestaurants:[Restaurant] = []
    var visibleRestaurants:[Restaurant] = []
    // Send a publish/subscribe message to reload the map view.
    func reloadMapView() {
        let name = Notification.Name("UpdateDisplayedMapPins")
        NotificationCenter.default.post(name: name, object:nil)
    }
    
    static func filterVisibleRestaurants(withClosure:(Restaurant) -> Bool) {
        Restaurants.shared.visibleRestaurants = Restaurants.shared.allRestaurants.filter(withClosure)
    }
    
    // Download our restaurants stored on backendless. // Randall & Joey
    static func downloadRestaurants() {
        let backendless = Backendless.sharedInstance()!
        var restaurantDataStore:IDataStore!
        restaurantDataStore = backendless.data.of(Restaurant.self)
        let qb = DataQueryBuilder()
        qb!.setRelated(["type"]) // also pull type column
        qb!.setWhereClause("price >= 0")
        let restaurants2:[Restaurant] = restaurantDataStore.find(qb) as! [Restaurant]
        Restaurants.shared.allRestaurants = restaurants2
        Restaurants.shared.visibleRestaurants = Restaurants.shared.allRestaurants
        Restaurants.shared.reloadMapView()
    }
    
    // Find a restaurant from our list of restaurants based on its name // Joey
    func findRestaurantByName(name:String) -> Restaurant {
        for restaurant in allRestaurants {
            if restaurant.name == name {
                return restaurant
            }
        }
        return Restaurant()
    }
    
    // Suresh // Joey
    static func getRestaurantStatus(restaurant: Restaurant) -> String {
        let currentDate = Date()
        let calendar = Calendar.current
        // This will give day of the week, 0 as sunday, 6 as saturday
        let dayOfWeek:Int = calendar.ordinality(of: .day, in: .weekOfMonth, for: currentDate)! - 1
        
        
        let isRestaurantOpen = restaurant.isOpen
        var currentStatus = isRestaurantOpen ? "Open. " : " - Closed. "
        if restaurant.alwaysOpen {
            currentStatus += "Open 24 Hours"
        } else {
            currentStatus = currentStatus + "Hours: \(restaurant.hours[dayOfWeek]![0]) - \(restaurant.hours[dayOfWeek]![1])"
        }
        return currentStatus
    }
    
    static func isRestaurantOpen(restaurant:Restaurant) -> Bool{
        let currentDate = Date()
        let calendar = Calendar.current
        let currentHour = calendar.component(.hour, from: currentDate)
        let currentMinute = calendar.component(.minute, from: currentDate)
        // This will give day of the week, 0 as sunday, 6 as saturday
        let dayOfWeek:Int = calendar.ordinality(of: .day, in: .weekOfMonth, for: currentDate)! - 1
        
        
        if restaurant.alwaysOpen {
            return true
        }
        // Get the restaurant opening time in hours and minutes
        let restaurantOpenTime = String(restaurant.hours[dayOfWeek]![0]) // Open Time
        let restaurantOpenHour: Int = Int((restaurantOpenTime.count == 3 ? restaurantOpenTime[0..<1] : restaurantOpenTime[0..<2]))!
        let restaurantOpenMinutes: Int = Int((restaurantOpenTime.count == 3 ? restaurantOpenTime[1..<3] : restaurantOpenTime[2..<4]))!
        
        // Get restaurant closing time in hours and minutes
        let restaurantClosingTime = String(restaurant.hours[dayOfWeek]![1]) // Close Time
        let restaurantClosingHour: Int = Int((restaurantClosingTime.count == 3 ? restaurantClosingTime[0..<1] : restaurantClosingTime[0..<2]))!
        let restaurantClosingMinutes: Int = Int((restaurantClosingTime.count == 3 ? restaurantClosingTime[1..<3] : restaurantClosingTime[2..<4]))!
        
        // Return if the restaurant will be open or not based on hours and minutes of open and closing.
        if currentHour > restaurantOpenHour && currentHour < restaurantClosingHour {
            return true
        } else if (currentHour == restaurantOpenHour) {
            return currentMinute >= restaurantOpenMinutes
        } else if (currentHour == restaurantClosingHour) {
            return currentMinute < restaurantClosingMinutes
        }
        
        // return false by default
        return false
    }
    
    // So it can't be initialized elsewhere...
    private init() {}
}
