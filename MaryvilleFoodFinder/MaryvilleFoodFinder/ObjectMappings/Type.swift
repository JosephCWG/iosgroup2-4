//
//  Type.swift
//  MaryvilleFoodFinder
//
//  Created by Joseph on 4/5/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import Foundation

@objcMembers
class Type: NSObject {
    // This is the type class which basically describes what food type the restaurant is.
    // Fast food, american diner, truck stop, ect.
    // this is pulled from backendless and paired to the restaurant.
    override var description: String {
        return "Name: \(name)"
    }
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    convenience override init() {
        self.init(name: "UNK")
    }
}
