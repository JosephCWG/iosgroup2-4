//
//  PointOfInterest.swift
//  MaryvilleFoodFinder
//
//  Created by Joseph on 2/18/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import Foundation
import MapKit

@objcMembers
class Restaurant: NSObject {
    // Standard description
    override var description: String {
        return "Name: \(name), lat: \(lat), lon: \(lon), rating: \(rating), price: \(price), alwaysOpen: \(alwaysOpen), hours: \(hours), phoneNumber: \(phoneNumber)"
    }
    
    var name:String
    var lat:Double
    var lon:Double
    var rating: Double
    var price: Double
    // This is in order to make it JSON convertable
    // Tried to use an enum but couldn't get it working correctly.
    // Int is day of week. (0 = Sunday)
    // array[0] is open hour in military time.
    // array[1] is close hour in military time.
    var alwaysOpen: Bool
    var objectId:String?
    //rawHours is a string representation of a json object representing hours
    var rawHours:String
    //parsed hours
    var hours:[Int: [Int]] {
        do {
            let decoder:JSONDecoder = JSONDecoder()
            let jsonData = rawHours.data(using: .utf8)!
            return try decoder.decode([Int:[Int]].self, from: jsonData)
        } catch {
            //JSON error, assume always open
            self.alwaysOpen = true
            return [1:[0,0],2:[0,0],3:[0,0],4:[0,0],5:[0,0],6:[0,0],7:[0,0]]
        }
    }
    var image:String
    var type:Type
    var phoneNumber:String
    var descText:String
    var menuLink:String
    
    //Coordinates for map pins. CLLocationDegrees is a wrap around a double object.
    // it was done like this so you can pass in a double from the database
    var twoDimensionalCoordinate:CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon))
        }
    }
    
    var isOpen:Bool {
        get {
            
            
            return true
        }
    }
    
    // Initializer needed for backendless
    convenience override init (){
        self.init(name:"", lat:0.0, lon:0.0, rating:0.0, price:0.0, alwaysOpen:true, rawHours:"", image:"default.jpg", phoneNumber:"0", type: Type(), descText:"", menuLink:"")
    }
    
    // Regular initializer
    init (name:String, lat:Double, lon:Double, rating:Double, price:Double, alwaysOpen:Bool, rawHours:String, image:String, phoneNumber:String, type:Type, descText:String,menuLink:String){
        self.name = name
        self.lat = lat
        self.lon = lon
        self.rating = rating
        self.price = price
        self.alwaysOpen = alwaysOpen
        self.rawHours = rawHours
        self.image = image == "" ? "default.jpg" : image
        self.phoneNumber = phoneNumber
        self.type = type
        self.descText = descText
        self.menuLink = menuLink
    }
}
