//
//  SecondViewController.swift
//  MaryvilleFoodFinder
//
//  Created by Joseph on 2/14/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredFoods = foodTypes.filter({( food : Food) -> Bool in
            return food.name.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    struct Food {
        var name:String
    }
    let searchController = UISearchController(searchResultsController: nil)
    var foodTypes:[Food] = []
    var filteredFoods = [Food]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellOptional = tableView.dequeueReusableCell(withIdentifier: "food")
        let cell = cellOptional!
        cell.textLabel?.text = foodTypes[indexPath.row].name
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        foodTypes.append(Food(name: "food1"))
        foodTypes.append(Food(name: "food2"))
        foodTypes.append(Food(name: "food3"))
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }


}

