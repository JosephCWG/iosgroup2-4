//
//  RestaurantTableViewController.swift
//  MaryvilleFoodFinder
//
//  Created by Student on 3/10/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import UIKit

class RestaurantTableViewController: UITableViewController,UISearchBarDelegate {

    @IBOutlet weak var searchview: UISearchBar!
    var restaurants:[String] = []
    var filterdata:[String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restaurants = Restaurants.shared.allRestaurants.map {$0.name}
        searchview.delegate = self
        filterdata = restaurants

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterdata = searchText.isEmpty ? restaurants : restaurants.filter { $0.contains(searchText) }
        tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdata.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurants", for: indexPath)
        if filterdata.count != 0
        {
            cell.textLabel?.text = filterdata[indexPath.row]
            cell.detailTextLabel?.text = Restaurants.getRestaurantStatus(restaurant: Restaurants.shared.findRestaurantByName(name: filterdata![indexPath.row]))
        } else{
            cell.textLabel?.text = restaurants[indexPath.row]
            cell.detailTextLabel?.text = Restaurants.getRestaurantStatus(restaurant: Restaurants.shared.allRestaurants[indexPath.row])
            
        }
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        let restaurantDVC = segue.destination as! RestaurantDetailViewController
        // Pass the selected object to the new view controller.
    restaurantDVC.restaurant = Restaurants.shared.findRestaurantByName(name: filterdata[tableView.indexPathForSelectedRow?.row ?? 0])
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Segue to the second view controller
        self.performSegue(withIdentifier: "detailSegue", sender: self)
    }
}
